import java.io.File
import java.nio.file.{Files, Paths}
import scala.io.Source

object FIleReader {
  def main(args: Array[String]): Unit = {
    readFile("test2.csv")
    readFile("test3.csv")
    "\n".getBytes().foreach(print)
    println()
    "\r".getBytes().foreach(print)

  }

  def readFile(path: String): Unit = {
//    val reader = Source.fromFile(path)
//    reader.foreach(print)
    Files.readAllBytes(Paths.get(path)).foreach(print)
    println()
  }
}
