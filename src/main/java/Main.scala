import org.apache.parquet.format.Util
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.{Encoders, Row, SparkSession}

object Main {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    //    spark.conf.set("spark.default.parallelism", 4)
    //        val size = 100000000
    //        val df = spark.range(1,size).repartition(4)
    //        df.write.mode("overwrite").csv("test.csv")
    var df = spark.read
      .option("inferSchema", false)
      .csv("test.csv")
    val limit = 100
//    import spark.implicits._
    df = df.mapPartitions { iter => iter.take(limit) }(RowEncoder.apply(df.schema)).toDF().limit(limit)
//    df = df.mapPartitions{iter => iter.take(limit).map(row => row.mkString(","))}(Encoders.STRING).toDF().limit(limit)
//    df = df.mapPartitions(iter => iter.take(limit).map(row => row.mkString(","))).toDF().limit(limit)
    df.write
      .option("header", "true")
      .option("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")
      .mode("overwrite")
      .csv("111.txt")
    Thread.sleep(100000)
    df.show()

  }
}