import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.SparkSession

import java.io.{FileInputStream, InputStreamReader}
import scala.io.{Codec, Source}

object CsvReaderMistach {

  def main(args: Array[String]) {
    val spark = SparkSession.builder().master("local").getOrCreate()
    val path = "/Users/chenankang/Downloads/CSL library modified testGbk (1).csv"
    //    val path = "./test2.csv"


    val df = spark.read
      .option("header", "true")
      .option("sep", ",")
      .option("encoding", "GBK")
      .option("inferSchema", "false")
      .option("timestampFormat", "yyyy/MM/dd HH:mm:ss ZZ")
      .option("quote", "\"")
      .option("escape", "\\")
      .option("multiLine", "true")
      .csv(path)
    df.show()

  }

//  csv.flatMap { lines =>
//    val path = new Path(lines.getPath())
//    UnivocityParser.tokenizeStream(
//      CodecStreams.createInputStreamWithCloseResource(lines.getConfiguration, path),
//      shouldDropHeader = false,
//      new CsvParser(parsedOptions.asParserSettings),
//      encoding = parsedOptions.charset)
//  }.take(1).headOption match {
//    case Some(firstRow) =>
//      val caseSensitive = sparkSession.sessionState.conf.caseSensitiveAnalysis
//      val header = CSVUtils.makeSafeHeader(firstRow, caseSensitive, parsedOptions)
//      val tokenRDD = csv.flatMap { lines =>
//        UnivocityParser.tokenizeStream(
//          CodecStreams.createInputStreamWithCloseResource(
//            lines.getConfiguration,
//            new Path(lines.getPath())),
//          parsedOptions.headerFlag,
//          new CsvParser(parsedOptions.asParserSettings),
//          encoding = parsedOptions.charset)
//      }
//      val sampled = CSVUtils.sample(tokenRDD, parsedOptions)
//      SQLExecution.withSQLConfPropagated(sparkSession) {
//        new CSVInferSchema(parsedOptions).infer(sampled, header)
//      }
//    case None =>
//      // If the first row could not be read, just return the empty schema.
//      StructType(Nil)
//  }

}
