import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path

import java.io.{File, RandomAccessFile}
import java.nio.channels.FileChannel
import scala.language.postfixOps

object FileOperation {

  def main(args: Array[String]): Unit = {
//    val path = new Path("test.csv")
//    val fs = path.getFileSystem(new Configuration())
//    val files = fs.listStatus(path)
//    files.foreach(e => println(e.getPath.getName))

//    val cf = fs.getContentSummary(path)
    val file = new File("text.txt")
    if(file.exists) {
      file.delete()
    }
    file.createNewFile()
    val str1 = "hello world\n"
    val str2 = "你好，世界\n"
    val l1 = str1.getBytes.length
    val l2 = str2.getBytes.length
    val s1 = 1000
    val s2 = 1000
    val fc1 = new RandomAccessFile("text.txt","rw").getChannel
    val fc2 = new RandomAccessFile("text.txt","rw").getChannel
    val out1 = fc1.map(FileChannel.MapMode.READ_WRITE,0,l1 * s1)
    val out2 = fc2.map(FileChannel.MapMode.READ_WRITE,l1 * s1,l2 * s2)
    for (_ <- 0 until s2) {
      out2.put(str2.getBytes)
    }
    for (_ <- 0 until s1) {
      out1.put(str1.getBytes)
    }
  }
}
