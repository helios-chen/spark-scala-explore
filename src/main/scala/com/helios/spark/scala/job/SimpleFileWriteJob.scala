package com.helios.spark.scala.job

import org.apache.spark.sql.SparkSession

object SimpleFileWriteJob {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("simple file write").getOrCreate()
    val size = 1000
    val df = spark.range(1, size, 1, 4)
    df.write
      .option("header","true")
      .mode("overwrite")
      .csv("/user/test/spark/simple01")
  }
}
