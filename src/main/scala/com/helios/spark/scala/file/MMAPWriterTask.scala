package com.helios.spark.scala.file

import java.io.{File, FileInputStream, RandomAccessFile}
import java.net.URI
import java.nio.channels.FileChannel

class MMAPWriterTask(src: URI, dest: URI, start: Long, bufSize: Int) extends Runnable {
  override def run(): Unit = {
    try {
      val file = new File(src)
      val fc = new RandomAccessFile(dest.getPath, "rw").getChannel
      val out = fc.map(FileChannel.MapMode.READ_WRITE, start, start + file.length())
      val in = new FileInputStream(file)
      val buf = new Array[Byte](bufSize)
      var len = in.read(buf)
      while (len > 0) {
        out.put(buf, 0, len)
        len = in.read(buf)
      }
      in.close()
      fc.close()
    }
    catch {
      case e: Throwable => throw e
    }

  }

}
