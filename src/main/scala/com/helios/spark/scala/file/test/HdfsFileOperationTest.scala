package com.helios.spark.scala.file.test

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

import java.nio.charset.StandardCharsets

object HdfsFileOperationTest {
  def main(args: Array[String]): Unit = {
    val conf = new Configuration()
    conf.set("fs.defaultFs", "hdfs://localhost:9000")
    val fs = FileSystem.get(conf)
    val path = new Path("/user/test/tmp/test01.txt")

    //文件写入
    val out = fs.create(path)
    val str = "hello world!!!\n"
    out.write(str.getBytes(StandardCharsets.UTF_8))
    out.close()

    //文件读取
    val in = fs.open(path)
    val bytes = new Array[Byte](1024)
    val len = in.read(bytes)
    println(new String(bytes, 0, len))
    in.close()
  }
}
