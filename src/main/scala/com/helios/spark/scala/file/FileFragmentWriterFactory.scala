package com.helios.spark.scala.file

import com.helios.spark.scala.file
import com.helios.spark.scala.file.FileWriterMode.{FileWriterMode, MMAP, RAF, SEQUENTIAL, SEQUENTIAL_RAF}

import java.io.{File, FileOutputStream, RandomAccessFile}
import java.net.URI
import java.util.concurrent.{LinkedBlockingQueue, ThreadPoolExecutor, TimeUnit}

object FileWriterMode extends Enumeration {
  type FileWriterMode = Value
  val MMAP: file.FileWriterMode.Value = Value("1")
  val RAF: file.FileWriterMode.Value = Value("2")
  val SEQUENTIAL: file.FileWriterMode.Value = Value("3")
  val SEQUENTIAL_RAF: file.FileWriterMode.Value = Value("4")
}

object FileFragmentWriterFactory {

  private val QUEUE_SIZE = 10000
  private val RATE = 0.1
  private val WAIT_TIME = 10
  private  val THREAD_NUMS = 32
  private val executor = new ThreadPoolExecutor(THREAD_NUMS, THREAD_NUMS, 1, TimeUnit.MINUTES, new LinkedBlockingQueue[Runnable]
  (QUEUE_SIZE))

  private class MMAPWriterTaskWrapper(src: URI, dest: URI, start: Long, bufSize: Int) extends
    MMAPWriterTask(src, dest, start, bufSize) {
    override def run(): Unit = {
      super.run()
      println(s"src file:${src.getPath}\tstart:$start")
    }
  }

  private class RAFWriterTaskWrapper(src: URI, dest: URI, start: Long, bufSize: Int) extends
    RAFWriterTask(src, dest, start, bufSize) {
    override def run(): Unit = {
      super.run()
      println(s"src file:${src.getPath}\tstart:$start")
    }
  }

  private class FileSequentialWriterTaskWrapper(src: URI, dest: FileOutputStream, bufSize: Int) extends
    FileSequentialWriterTask(src, dest, bufSize) {
    override def run(): Unit = {
      super.run()
      println(s"src file:${src.getPath}")
    }
  }

  sealed class FileElement(val path: URI, val start: Long)

  def merge(mode: FileWriterMode, files: Seq[FileElement], dest: URI, bufSize: Int)
  : Unit
  = {
    mode match {
      case SEQUENTIAL =>
        val out = new FileOutputStream(new File(dest.getPath))
        files.foreach(f => new FileSequentialWriterTaskWrapper(f.path, out, bufSize).run())
        out.close()
      case SEQUENTIAL_RAF =>
        val out = new RandomAccessFile(dest.getPath, "rw")
        files.foreach(f => new SequentialRAFWriterTask(f.path, out, bufSize).run())
        out.close()
      case _ =>
        files.map(f => {
          while (executor.getQueue.remainingCapacity() <= QUEUE_SIZE * RATE) {
            TimeUnit.MILLISECONDS.sleep(WAIT_TIME)
          }
          executor.submit(build(mode, f.path, dest, bufSize, f.start))
        })
          .foreach(f => f.get())
    }
  }

  def close(): Unit = executor.shutdown()

  private def build(mode: FileWriterMode, src: URI, dest: URI, bufSize: Int, start: Long = 0)
  : Runnable = {
    mode match {
      case MMAP => new MMAPWriterTaskWrapper(src, dest, start, bufSize)
      case RAF => new RAFWriterTaskWrapper(src, dest, start, bufSize)
      case _ => new Runnable {
        override def run(): Unit = {}
      }
    }
  }
}


