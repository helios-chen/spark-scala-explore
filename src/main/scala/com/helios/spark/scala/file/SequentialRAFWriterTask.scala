package com.helios.spark.scala.file

import java.io.{File, FileInputStream, FileOutputStream, RandomAccessFile}
import java.net.URI

class SequentialRAFWriterTask(src: URI, out: RandomAccessFile, bufSize: Int) extends Runnable {
  override def run(): Unit = {
    try {
      val in = new FileInputStream(new File(src))
      val buf = new Array[Byte](bufSize)
      try {
        var len = in.read(buf)
        while (len != -1) {
          out.write(buf, 0, len)
          len = in.read(buf)
        }
        println(s"src file:${src.getPath}")
      }
      finally {
        in.close()
      }
    }
    catch {
      case e: Throwable => throw e
    }
  }
}

