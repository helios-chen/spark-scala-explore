package com.helios.spark.scala.file.test

import com.helios.spark.scala.file.FileFragmentWriterFactory.FileElement
import com.helios.spark.scala.file.{FileFragmentWriterFactory, FileWriterMode}

import java.io.File

object FileMergeTest {
  def main(args: Array[String]): Unit = {
    val srcDir = "../resource/files/"
    val bufSize = 10240
    val file = new File(srcDir)
    val files = file.listFiles()
    var offset = 0L
    val sortedFiles = files.map(f => (f.getName.substring(3).toInt, f))
      .sortBy(p => p._1)
      .map(p => {
        val e = new FileElement(p._2.toPath.toUri, offset)
        offset += p._2.length()
        e
      })
//      .sortBy(_.start)
//      .reverse
          .filter(_.path.getPath.split("/").last.substring(3).toInt <= 2000)
    val start = System.currentTimeMillis
    //MMAP方式不能超过Int.MAX_VALUE
        testMMAP(sortedFiles,bufSize)
//    testRAF(sortedFiles, bufSize)
//    testSequentialRAF(sortedFiles, bufSize)
//        testSequential(sortedFiles,bufSize)
    println(s"total cost:${(System.currentTimeMillis - start) / 1000}s")
    FileFragmentWriterFactory.close()

  }

  private def testMMAP(files: Seq[FileElement], bufSize: Int): Unit = {
    val dest = new File("../resource/bigfile_mmap").toPath.toUri
    FileFragmentWriterFactory.merge(FileWriterMode.MMAP, files, dest, bufSize)

  }

  private def testRAF(files: Seq[FileElement], bufSize: Int): Unit = {
    val dest = new File("../resource/bigfile_raf").toPath.toUri
    FileFragmentWriterFactory.merge(FileWriterMode.RAF, files, dest, bufSize)
  }

  private def testSequentialRAF(files: Seq[FileElement], bufSize: Int): Unit = {
    val dest = new File("../resource/bigfile_raf_seq").toPath.toUri
    FileFragmentWriterFactory.merge(FileWriterMode.SEQUENTIAL_RAF, files, dest, bufSize)
  }

  private def testSequential(files: Seq[FileElement], bufSize: Int): Unit = {
    val dest = new File("../resource/bigfile_sequential").toPath.toUri
    FileFragmentWriterFactory.merge(FileWriterMode.SEQUENTIAL, files, dest, bufSize)
  }
}
