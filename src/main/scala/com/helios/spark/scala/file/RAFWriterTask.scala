package com.helios.spark.scala.file

import java.io.{File, FileInputStream, RandomAccessFile}
import java.net.URI

class RAFWriterTask(src: URI, dest: URI, start: Long, bufSize: Int) extends Runnable {
  override def run(): Unit = {
    val rf = new RandomAccessFile(dest.getPath, "rw")
    val in = new FileInputStream(new File(src))
    val buf = new Array[Byte](bufSize)
    var len = in.read(buf)
    val start = System.currentTimeMillis
    rf.seek(start)
    while (len > 0) {
      rf.write(buf, 0, len)
      len = in.read(buf)
    }
    in.close()
    rf.close()
  }

}
