package com.helios.spark.scala.file

import java.io.{File, PrintWriter}
import scala.language.postfixOps
import scala.util.Random


object RandFileGenerator {
  def main(args: Array[String]): Unit = {
    val amount = 10000
    for (i <- 0 until amount) {
      val path = s"../resource/files/000$i"
      val file = new File(path)
      println(file.getName)
      if (file.exists) {
        file.delete()
      }
      file.createNewFile()
      val writer = new PrintWriter(file)
      val lines = 10000
      val cols = 30
      val mod = 1000
      writer.write(path)
      for (r <- 0 until lines) {
        writer.print(s"${file.getName}_${r + 1}:")
        for (_ <- 0 until cols) {
          writer.print(s"${Random.nextInt(mod)} ")
        }
        writer.println()
      }
      writer.close()
    }
  }
}
